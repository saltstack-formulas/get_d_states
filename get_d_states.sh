#!/bin/bash

cmdline=`ps aux |awk -v total=0 '$8 == "D" {print;}'`
total=`echo $cmdline |wc -l`

if [ "$cmdline" != "" ]; then
  if [[ "$1" == "count" ]] ;then
    echo $total
  else
    echo $cmdline
  fi
else
  total=0
  if [[ "$1" == "count" ]] ;then
      echo $total
  fi
fi
