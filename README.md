# get_d_states

> Useless since SaltStack 3006.0 and the [PR I created](https://github.com/saltstack/salt/pull/61421) to the module ps.status

SaltStack formula to get process in D states.