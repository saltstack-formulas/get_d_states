copying get_d_states file:
  file.managed:
    - source: salt://get_d_states/get_d_states.sh
    - name: /usr/local/sbin/get_d_states
    - user: root
    - group: root
    - mode: '0744'
